﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question07.Console
{
    public class StationCommunicatorOrchestrator
    {
        private readonly SpatialCommunication service;
        private readonly TextWriter textWriter;

        public StationCommunicatorOrchestrator(TextWriter textWriter)
        {
            this.textWriter = textWriter;
            this.service = new SpatialCommunication(textWriter);
        }

        public async Task ManageCommunication()
        {
            throw new NotImplementedException();
        }
    }
}
