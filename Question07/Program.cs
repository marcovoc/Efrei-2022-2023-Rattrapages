﻿using Question07.Console;

var spatialCommunicator = new StationCommunicatorOrchestrator(Console.Out);
await spatialCommunicator.ManageCommunication();