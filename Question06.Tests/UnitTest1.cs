using Question06.Console;

namespace Question08.Tests;

public class TargetChooserTests
{
    [Fact]
    public void WriteTop5Criminals()
    {
        var expected = @"Vivienne  Bourdillon (born 01/05/1989 in Paris)
-> 607041 in Paris
-> 910187 in Berlin
Dora  Ramires (born 23/02/2018 in Lisbon)
-> 131702 in London
-> 720144 in Lisbon
-> 283208 in Lisbon
Petunia  Kingston (born 22/02/1975 in London)
-> 858454 in Berlin
-> 246372 in Paris
Karter  Farran (born 05/09/2005 in London)
-> 904533 in Lisbon
Ernest  Julien (born 27/04/1986 in Paris)
-> 821578 in Paris
";

        var stringWriter = new StringWriter();
        var chooser = new TargetChooser(stringWriter);
        chooser.WriteTop5Criminals(Data.Bounties);

        Assert.Equal(expected, stringWriter.ToString());
    }
}